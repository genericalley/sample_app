require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "",
                                         email: "user@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end
  
  test "valid signup information" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name: "Kelly Clarkson",
                                         email: "squiggly@comcast.net",
                                         password:              "coklsoftball",
                                         password_confirmation: "coklsoftball" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert test_logged_in?
  end
  
end