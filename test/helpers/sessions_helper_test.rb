require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:michael)
    remember(@user)
  end
  
  test "current_user returns right user when seesion is nil" do
    assert_nil session[:user_id]
    assert_equal @user, current_user
    assert test_logged_in?
  end
  
  test "current_user returns nil when remember_digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
end